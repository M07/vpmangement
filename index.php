<?php include './control.php' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
	<head>
		<title>Docapost BPO : Réservation</title>
		<link rel="shortcut icon" type="image/x-icon" href="img/logo.png" />
		<meta charset="utf-8"/>
		
		<link href="css/datepicker.css" rel="stylesheet"/>
		<link href="css/bootstrap.css" rel="stylesheet"/>
		<link href="css/custom.css" rel="stylesheet"/>
		<link href='css/fullcalendar.css' rel='stylesheet' />
		<link href='css/fullcalendar.print.css' rel='stylesheet' media='print' />
        <link href="css/jquery.gritter.css" rel="stylesheet"/>
		<link href="css/bootstrap-timepicker.min.css" rel='stylesheet' />
		<link href="css/bootstrap-select.min.css" rel='stylesheet' />
		
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src='js/fullcalendar.js'></script>
		<script type="text/javascript" src="js/bootstrap.js"></script>
		<script type="text/javascript" src="js/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="js/bootstrap-timepicker.js"></script>
        <script type="text/javascript" src="js/bootstrap-select.min.js"></script>
        <script type="text/javascript" src="js/jquery.gritter.js"></script>
        <script type="text/javascript" src="js/date.js"></script>
		
		<?php 
          	if (!isset($_SESSION['authenticatedUser'])) { ?>
          		<script type="text/javascript" src="js/cal.js"></script>
          	<?php
          	} else { ?>
          		<script type="text/javascript" src="js/adminCal.js"></script>
          	<?php
          	}
        ?>
	</head>
	<body data-spy="scroll" data-target=".bs-docs-sidebar">
		<div class="navbar navbar-inverse navbar-fixed-top">
	      <div class="navbar-inner">
	        <div class="container">
	          <title>Réservation Vidéoprojecteur</title>
	          <?php 
	          	if (!isset($_SESSION['authenticatedUser'])) { 
	          		echo "<a id='login' class='brand' href='#login-dialog' role='button' data-toggle='modal'>Connexion</a>";	          		
	          		echo "<a id='help' class='brand' href='#help-dialog' role='button' data-toggle='modal'>Comment réserver?</a>";
	          	} else {
	          		echo "<button type='button' class='btn' onclick ='javascript:adminLogout()'>Déconnexion</button>";
	          		echo "<span class='welcome'>Bienvenue ".$_SESSION['authenticatedUser']."</span>";
	          	}
	          ?>
	        </div>
	      </div>
	    </div>
	    <div class="container">
		    <div class="row">
		    	<div id="center-page">
			    	<div id="page">
			    		<div class="input-append date inputDate" id="inputDate" data-date="12/02/2012" data-date-format="dd/mm/yyyy">
					  		<input class="span2" size="16" type="text">
					  		<span class="add-on"><i class="icon-th"></i></span>
						</div>
						<div id='calendar'/>
					</div>
				</div>
	    	</div>		
	   	</div>
		<footer class="footer">
	      <div class="container">
	        <p>Designed and built by <a href="mailto:marc.ternisien3@docapost-bpo.com">Ternisien Marc</a></p>
	      </div>
	    </footer>
	    <!-- Login Dialog -->
	    <div id="login-dialog" class="modal hide fade" tabindex="-1" role="dialog">
		  <div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal">x</button>
		    <h3>Connexion admin</h3>
		  </div>
		  <div class="modal-body">
		    <form method="post" name="login_form">
		      <p><input type="text" class="span3" id="username" placeholder="Nom d'admin"></p>
		      <p><input type="password" class="span3" id="passwd" placeholder="Mot de passe"></p>
		      <button type="button" class="btn btn-primary" onclick ="javascript:adminLogin()">Connexion</button>
		      <button class="btn" data-dismiss="modal" aria-hidden="true">Annuler</button>
		    </form>
		  </div>
		</div>
		 <!-- Booking Dialog -->
		<div id="reserve-dialog" class="modal hide fade" tabindex="-1" role="dialog">
		  <div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal">x</button>
		    <h3 id="reserveTitle">Ajouter une réservation</h3>
		  </div>
		  <div class="modal-body">
		    <form id="bookingfForm" method="post" name="reserve_form">
		      <div><input id="borrowerName" type="text" class="span3" name="fBorrowerName" placeholder="Nom de l'emprunteur" required="true"></div>
		      <input type="hidden" id="hiddenId" name="fBookingId" type="integer">
		      <div>
		      	<h5>Date :</h5>
		      	<span id="infoWeekDay" class="input-small uneditable-input"></span>
		      	<input type="hidden" id="hiddenWeekDay" name="fWeekDay" type="text">
		      </div>
		      <div id="dialogTimeContainer">
		      	<div class="dialogTime">
		      		<h5>Heure début :</h5>
					<div class="input-append bootstrap-timepicker">
						<input id="timepickerStart" type="text" class="input-small" name="fStartTime">
						<span class="add-on"><i class="icon-time"></i></span>
					</div>
				</div>
				<div class="dialogTime">
					<h5>Heure de fin :</h5>	            
					<div class="input-append bootstrap-timepicker">
						<input id="timepickerEnd" type="text" class="input-small" name="fEndTime">
						<span class="add-on"><i class="icon-time"></i></span>
					</div>
				</div>
		      </div>		      	
			  <p>
		      	<select id="roomSelect" class="selectpicker show-tick" name="fRoom">
		      		<?php showRooms() ?>
		      	</select>
				<select id="projectorSelect" class="selectpicker show-tick" name="fProjector">
				    <?php showProjectors() ?>
				</select>
		      </p>		      
			  <p style="margin-top:20px">
			  	<button id="reserveButton" type="button" class="btn btn-primary" onclick ="javascript:addOrChangeBooking()">Ajouter</button>
			  	<button id="deleteButton" type="button" class="btn btn-danger hide" onclick ="javascript:deleteBooking()">Supprimer</button>
			  	<button class="btn" data-dismiss="modal" aria-hidden="true">Annuler</button>
			  </p>
		    </form>
		  </div>
		</div>
		 <!-- Help Dialog -->
		<div id="help-dialog" class="modal hide fade" tabindex="-1" role="dialog">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h3>Comment réserver?</h3>
			</div>
			<div class="modal-body">
				<p>Seules les personnes connectées peuvent ajouter ou modifier des réservations. Veuillez donc <b>contacter l'accueil</b> pour faire votre réservation.</p>
				<p>Contact : <a href="mailto:videoprojecteur.sophia@docapost-bpo.com">videoprojecteur.sophia@docapost-bpo.com</a>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">Fermer</button>
			</div>
		</div>
		<script type='text/javascript' src='js/custom.js'></script>		
	</body>
</html>