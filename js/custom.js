/**
 *
 * Custom part
 * Author: Ternisien Marc
 * 
 */

 //Transform date to dd/mm/yyyy
function formatDate(myDate) {
	var dd = myDate.getDate();
	var mm = myDate.getMonth()+1; //January is 0!

	var yyyy = myDate.getFullYear();
	if(dd<10){dd='0'+dd} 
	if(mm<10){mm='0'+mm}
	return dd+'/'+mm+'/'+yyyy;
}
//Add some minutes to a time passed in parameters
function addMinutes(time, szMinutes) {
	var timeArray;
	var minutes = parseInt(szMinutes);
	timeArray = time.split(':');
	var tHours = parseInt(timeArray[0], 10);
	var tMinutes = parseInt(timeArray[1], 10);
	if(minutes > 0) {
		tMinutes += minutes;
		hoursInMinutes = (minutes/60)+1;
		if (tMinutes > 59) {
			tHours += hoursInMinutes;//I don't check if hours > 24 
			tMinutes = tMinutes - 60*hoursInMinutes;
		}
	} else {
		minutes *= -1;
		tMinutes -= minutes;
		hoursInMinutes = (minutes/60)+1;
		if (tMinutes < 0) {
			tHours -= hoursInMinutes;//I don't check if hours < 0 
			tMinutes = tMinutes + 60*hoursInMinutes;
		}
	}
    return tHours+":"+tMinutes;
}

//Get unix timestamp of date + time 
// date is formated d/M/y
// time is formated H:m
function getTime(date, time) {
	return getDateFromFormat(date +" "+time+":00", 'd/M/y H:m:s')/1000;
}

var nowTemp = new Date();
var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
var oneYearAfter = new Date(nowTemp.getFullYear()+1, nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
 
//Init the date picker
var checkin = $('#inputDate').datepicker({
	weekStart: 1,
  	onRender: function(date) {
    	return (date.valueOf() < now.valueOf() || date.valueOf() > oneYearAfter.valueOf()) ? 'disabled' : '';
  	}
}).datepicker('setValue', formatDate(nowTemp)).on('changeDate', function(ev){
	if (ev.date.valueOf() < now.valueOf() || ev.date.valueOf().valueOf() > oneYearAfter.valueOf()){
		$('#inputDate').datepicker('setValue', formatDate(now));
	} else {
		$('#calendar').fullCalendar('gotoDate', ev.date);
		window.location.hash =  ev.date.getTime();
	}
  }).data('datepicker');

// Get the url hash (a unix time stamp ) and send calendar to the right day
function goToHash() {
	if(window.location.hash) {
	  var hash = window.location.hash.substring(1); //Puts hash in variable, and removes the # character
      var origin = new Date().getTime();
      var hashDate = new Date(Number(hash));
      var view = $('#calendar').fullCalendar('getView');
      if(view.start > hashDate || view.end <= hashDate) {
      	$('#calendar').fullCalendar('gotoDate', hashDate);
      }
    }
}
//log in the user
function adminLogin() {
	var postData = {};
	postData["adminLogin"]=true;
	postData["username"]=$('#username').val();
	postData["passwd"]= $('#passwd').val();
	
	$.ajax({
		url:"index.php", 
		type:"POST",
		data: postData,
		success:function(data){		
			if(data) {		
				location.reload();
			} else {
				$('#passwd').val("");
				growl("Connexion refusée", "La combinaison login/password n'est pas correcte!");	
				for (var x = 1; x <= 3; x++) {
		            $('#login-dialog').animate({ left: "49%" }, 10).animate({ left: "50%" }, 50).animate({ left: "51%" }, 10).animate({ left: "50%" }, 50);
		        }
			}
		},
		error:function(a,b,c){
			console.log("error adminLogin");
		}
    });
}

// log out the user
function adminLogout() {
	$.post("index.php", { logout: true }, function() { location.reload() });
}

// delete a booking
function deleteBooking() {
	$.post("index.php", { removeReserve: true, bookingId: $('#hiddenId').val()}, function() { location.reload() });
}

// Not used, but allow to return the event of loaded calendar event if we pass in parameter the id
function getEventById(eventId) {
	var events = [];
	events = $('#calendar').fullCalendar('clientEvents'); // Get all events
	for (var i=0; i<events.length; i++) {
		if (events[i].id === eventId) {
			return events[i];
		}
	}
	return null;
}
// This method is called when click on the button to add or modify a booking
function addOrChangeBooking() {
	if(!$('#borrowerName').val()) {
		growl("Champs manquant", "Le <i>nom de l'emprunteur</i> est requis.");
		return;
	}
	var postData = {};
	var eventId =  $('#hiddenId').val();
	postData["bookingInformation"]=true;
	if(eventId) {
		postData["bookingId"]= Number(eventId);
	}
	postData["start"]= getTime($('#hiddenWeekDay').val(), $('#timepickerStart').val());
	postData["end"]= getTime($('#hiddenWeekDay').val(), $('#timepickerEnd').val());
	
	$.ajax({
		url:"index.php", 
		type:"POST",
		data: postData,
		dataType: 'json',
		success:function(obj){
			var roomList = obj.roomData;
			var projectorList = obj.projectorData;
			var bFail = false;
			if(roomList != null) {				
				for (var i = 0; i < roomList.length; i++) {
	    			var roomVal = "Salle "+roomList[i];
	    			if($('#roomSelect').val() === roomVal) {
	    				growl(roomVal+" indisponible", "La salle "+roomList[i]+" est réservée pour un autre créneau.");
	    				bFail = true;
	    			}
	    		}
			}
			if(projectorList != null) {
				for (var i = 0; i < projectorList.length; i++) {
	    			var projectorVal = "Projecteur "+projectorList[i];
	    			if($('#projectorSelect').val() === projectorVal) {
	    				growl(projectorVal+" indisponible", "Le vidéoprojecteur "+projectorList[i]+" est réservé pour un autre créneau.");
	    				bFail = true;
	    			}
	    		}
			}
			if(!bFail) {		
				$("form#bookingfForm").submit();
			} else {
				$('#reserve-dialog').modal('hide');
			}
		},
		error:function(a,b,c){
			console.log("error changeBooking");
		}
    });
}

// Show the dialog to modify a booking
function showChangeBooking(calEvent) {
	var postData = {};
	postData["bookingInformation"]=true;
	postData["bookingId"]=calEvent.id;
	postData["start"]=calEvent.start.getTime()/1000;
	postData["end"]=calEvent.end.getTime()/1000;
	$.ajax({
		url:"index.php", 
		type:"POST",
		data: postData,
		dataType: 'json',
		success:function(obj){
			var roomList = obj.roomData;
			var projectorList = obj.projectorData;
			var fDate = formatDate(calEvent.start);
			$('#reserveTitle').text("Modifier une réservation");
			$('#reserveButton').text("Modifier");
			$('#hiddenWeekDay').val(fDate);
			$('#infoWeekDay').text(fDate);
			$('#hiddenId').val(calEvent.id);
			$('#deleteButton').removeClass("hide");
			$('#borrowerName').val(obj.bookingData.borrowerName);
			$('#roomSelect option').removeAttr("disabled");
			if(roomList != null) {				
				for (var i = 0; i < roomList.length; i++) {
	    			var roomVal = "Salle "+roomList[i];
	    			$("#roomSelect option[value='"+roomVal+"']").attr("disabled", "disabled");
	    		}
			}
			$('#projectorSelect option').removeAttr("disabled");
			if(projectorList != null) {
				for (var i = 0; i < projectorList.length; i++) {
	    			var projectorVal = "Projecteur "+projectorList[i];
	    			$("#projectorSelect option[value='"+projectorVal+"']").attr("disabled", "disabled");
	    		}
			}
			$('#roomSelect').val("Salle "+ obj.bookingData.roomId);
			$('#roomSelect').change();
			$('#projectorSelect').val("Projecteur "+ obj.bookingData.projectorId);
			$('#projectorSelect').change();
			$('#timepickerStart').timepicker('setTime', calEvent.start.getHours()+":"+calEvent.start.getMinutes());
			$('#timepickerEnd').timepicker('setTime', calEvent.end.getHours()+":"+calEvent.end.getMinutes());
			$('#reserve-dialog').modal('show');
		},
		error:function(a,b,c){
			console.log("error showChangeBooking");
		}
    });
}

// Show the dialog to add a booking
function showAddBooking(start, end) {
	if(now > start) {
		growl("Créneau invalide", "Ce créneau est déjà passé.");
		return;
	}
	var postData = {};
	postData["bookingInformation"]=true;
	postData["start"]= start.getTime()/1000;
	postData["end"]= end.getTime()/1000;
	
	$.ajax({
		url:"index.php", 
		type:"POST",
		data: postData,
		dataType: 'json',
		success:function(obj){
			var roomList = obj.roomData;
			var projectorList = obj.projectorData;
			var fDate = formatDate(start);
			$('#reserveTitle').text("Ajouter une réservation");
			$('#reserveButton').text("Ajouter");
			$('#hiddenWeekDay').val(fDate);
			$('#infoWeekDay').text(fDate);
			$('#hiddenId').val("");
			$('#borrowerName').val("");
			$('#deleteButton').addClass("hide");
			$('#roomSelect option').removeAttr("disabled");
			if(roomList != null) {				
				for (var i = 0; i < roomList.length; i++) {
	    			var roomVal = "Salle "+roomList[i];
	    			$("#roomSelect option[value='"+roomVal+"']").attr("disabled", "disabled");
	    		}
			}
			$('#projectorSelect option').removeAttr("disabled");
			if(projectorList != null) {
				for (var i = 0; i < projectorList.length; i++) {
	    			var projectorVal = "Projecteur "+projectorList[i];
	    			$("#projectorSelect option[value='"+projectorVal+"']").attr("disabled", "disabled");
	    		}
			}
			if($('#projectorSelect option:not([disabled])').size() == 0) {
				growl("Vidéoprojecteurs indisponibles", "Tout les vidéoprojecteurs sont réservés pour ce créneau.");
			} else if($('#roomSelect option:not([disabled])').size() == 0) {
				growl("Salles indisponibles", "Toutes les salles sont réservées pour ce créneau.");
			} else {				
				$('#roomSelect').val($('#roomSelect option:not([disabled])').first().attr('value'));
				$('#roomSelect').change();
				$('#projectorSelect').val($('#projectorSelect option:not([disabled])').first().attr('value'));
				$('#projectorSelect').change();
				$('#timepickerStart').timepicker('setTime', start.getHours()+":"+start.getMinutes());
				$('#timepickerEnd').timepicker('setTime', end.getHours()+":"+end.getMinutes());
				$('#reserve-dialog').modal('show');
			}
		},
		error:function(a,b,c){
			console.log("error showAddBooking");
		}
    });
}
// Show a growl message 
function growl(title, text) {		
	var unique_id = $.gritter.add({
		title: title,
		text: text,
		image: './img/M07.png',
		// (bool | optional) if you want it to fade out on its own or just sit there
		sticky: true,
		time: '',
		class_name: 'my-sticky-class'
	});
	setTimeout(function(){
		$.gritter.remove(unique_id, {
			fade: true,
			speed: 'slow'
		});

	}, 6000);
}
			