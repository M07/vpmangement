/**
 *
 * Custom part
 * Author: Ternisien Marc
 * 
 */

$(document).ready(function() {
	var date = new Date();
	var d = date.getDate();
	var m = date.getMonth();
	var y = date.getFullYear();

	var calendar = $('#calendar').fullCalendar({
		header: {
			left: 'prev,today,next',
			center: 'title',
			right: ''
		},
		monthNames:['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
		monthNamesShort:['janv.','févr.','mars','avr.','mai','juin','juil.','août','sept.','oct.','nov.','déc.'],
		dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
		dayNamesShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
		titleFormat: {
		    month: 'MMMM yyyy',
		    week: "d[ MMMM][ yyyy]{ - d MMMM yyyy}",
		day: 'dddd d MMMM yyyy'
		},
		columnFormat: {
		    month: 'ddd',
		week: 'ddd d',
		day: ''
		},
		axisFormat: 'H:mm', 
		timeFormat: {
		    '': 'H:mm', 
		agenda: 'H:mm{ - H:mm}'
		},
		firstDay:1,
		allDaySlot: false,
		defaultView:'agendaWeek',
		weekends: false,
		selectable: true,
		selectHelper: true,
		ignoreTimezone: false,
		minTime: 7,
		maxTime: 21,
		eventSources: [
	        {
	            url: 'feed.php', // use the `url` property
	        }
    	],
		viewDisplay: function(view) {
			if($('.fc-button-today').hasClass("fc-state-disabled")) {
				$('.fc-button-prev').addClass("fc-state-disabled");
			} else {
				$('.fc-button-prev').removeClass("fc-state-disabled");
			}

			if(view.end > oneYearAfter) {
				$('.fc-button-next').addClass("fc-state-disabled");
			} else {
				$('.fc-button-next').removeClass("fc-state-disabled");
			}
			$('.fc-first .fc-widget-content').removeClass("notAllowed");
			$('.fc-first .fc-today').prevAll().addClass("notAllowed");
			var datePick = $('#inputDate').data('datepicker').date;
			if(view.start > datePick || view.end <= datePick) {
				var dateToshow = (view.start  > now ? view.start  : now)
				$('#inputDate').datepicker('setValue', formatDate(dateToshow));
				window.location.hash = dateToshow.getTime();
			}
	    },
		select: function(start, end, allDay) {
			showAddBooking(start, end);
		},
		eventClick: function(calEvent, jsEvent, view) {
			showChangeBooking(calEvent);
	    },
		editable: false
	});
	$('#timepickerStart').timepicker({
		showMeridian: false,
	}).on('changeTime.timepicker', function(e) {
		$('#timepickerEnd').timepicker('setMinTime', addMinutes(e.time.value, 30));//My own function to limit the min time of timepicker 
	});
	$('#timepickerEnd').timepicker({
		showMeridian: false
	}).on('changeTime.timepicker', function(e) {
		$('#timepickerStart').timepicker('setMaxTime', addMinutes(e.time.value, -30));//My own function to limit the max time of timepicker 
	});
	$('.selectpicker').selectpicker();
	$('.fc-agenda-slots td').addClass("fc-view-crosshair");
	goToHash();	
});