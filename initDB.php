<?php 			
	/* Init BD
	// Execute this script to reinit the db with the config values

	include 'config.php';
	$db = new SQLite3('db/mysqlitedb.db');
	echo "drop tables<br/>";
	$db->exec('DROP TABLE booking');
	$db->exec('DROP TABLE projector');
	$db->exec('DROP TABLE room');
	
	echo "create tables<br/>";
	$db->exec('CREATE TABLE room(
	  roomId    	INTEGER PRIMARY KEY
	)');
	$db->exec('CREATE TABLE projector(
	  projectorId	INTEGER PRIMARY KEY
	)');
	$db->exec('CREATE TABLE booking(
	  bookingId		INTEGER PRIMARY KEY,
	  borrowerName	TEXT,
	  startTime		LONG, 
	  endTime		LONG,
	  roomId 		INTEGER,
	  projectorId	INTEGER,
	  FOREIGN KEY(roomId) REFERENCES room(roomId),
	  FOREIGN KEY(projectorId) REFERENCES projector(projectorId)
	)');
	echo "insert values<br/>";
	
	foreach($roomList as $roomId) {
		echo "roomId ".$roomId."<br/>";
		$db->exec('INSERT INTO room VALUES ('.$roomId.')');
	}
	foreach($projectorList as $projectorId) {
		echo "projectorId ".$projectorId."<br/>";
		$db->exec('INSERT INTO projector VALUES ('.$projectorId.')');
	}
	echo "close db<br/>";
	$db->close();
	//*/
?>