<?php 
	include './config.php';
	ob_start();
	session_start();
	if(!isset($_SESSION["REMOVE_TIME"])) {
		//Each session, we check if can remove past events
		$today  = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
		$_SESSION["REMOVE_TIME"] = $today;
		$db = new SQLite3('db/mysqlitedb.db');				
		$statement = $db->prepare('DELETE FROM booking WHERE  startTime < :today');
		$statement->bindValue(':today', $today);
		$statement->execute();				
	}	
	
	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		//The post part
		$db = new SQLite3('db/mysqlitedb.db');
		if (!empty($_POST["adminLogin"])) {
			//We check if user can log in
			if($_POST["username"] === $usernameConf && $_POST["passwd"] === $passwordConf) {
				$_SESSION['authenticatedUser'] = $usernameConf;
				echo true;
			}
			echo false;
		} else if(!empty($_POST["logout"])) {
			// we log out the connected user
			unset($_SESSION);
			session_destroy();
			session_write_close();
		} else if(!empty($_POST["bookingInformation"])) {
			// We give some informations about a booking like the borrower name, the projector id , the room id, the room id list of other events, the projector id list of other events
			$roomData[] = null;
			$projectorData[] = null;
			if(!empty($_POST["bookingId"])) {
				//If we give the booking id, we want to have informations of an existing event
				$statement = $db->prepare('SELECT * FROM booking WHERE bookingId = :bookingId');
				$statement->bindValue(':bookingId', $_POST["bookingId"]);

				$results = $statement->execute();
				$row = $results->fetchArray();	
				$bookingData = array("borrowerName" => $row["borrowerName"], "roomId" => $row["roomId"], "projectorId" => $row["projectorId"]);

				$statement = $db->prepare('SELECT roomId, projectorId FROM booking WHERE bookingId <> :bookingId AND :start < endTime  AND :end > startTime');
				$statement->bindValue(':bookingId', $_POST["bookingId"]);
				$statement->bindValue(':start', $_POST["start"]);
				$statement->bindValue(':end', $_POST["end"]);
				$results = $statement->execute();
				while ($row = $results->fetchArray()) {	
					$roomData[] = $row["roomId"];
					$projectorData[] = $row["projectorId"];
				}

				echo json_encode(array("bookingData" => $bookingData, "roomData" => $roomData, "projectorData" => $projectorData));			
			} else {
				//Otherwise we just want to have global informations
				$statement = $db->prepare('SELECT roomId, projectorId FROM booking WHERE :start < endTime  AND :end > startTime');
				$statement->bindValue(':start', $_POST["start"]);
				$statement->bindValue(':end', $_POST["end"]);
				$results = $statement->execute();
				while ($row = $results->fetchArray()) {	
					$roomData[] = $row["roomId"];
					$projectorData[] = $row["projectorId"];	
				}
				echo json_encode(array("roomData" => $roomData, "projectorData" => $projectorData));	
			}

		} else {
			//We add a booking
			if (isset($_SESSION['authenticatedUser'])) { // Check if we have an authenticated admin */
				if (empty($_POST["removeReserve"])) {
					$borrowerName = $_POST["fBorrowerName"];
					$pattern = '/[^\d]/ ';
					$room = preg_replace($pattern, '', $_POST["fRoom"]);
					$projector = preg_replace($pattern, '', $_POST["fProjector"]);
					$weekDay = $_POST["fWeekDay"];
					$startTime = $_POST["fStartTime"];
					$startTimeD = DateTime::createFromFormat('d/m/Y H:i', $weekDay." ".$startTime);
					$startTime = $startTimeD->getTimestamp();
					$endTime = $_POST["fEndTime"];
					$endTimeD = DateTime::createFromFormat('d/m/Y H:i', $weekDay." ".$endTime);
					$endTime = $endTimeD->getTimestamp();

					if(!empty($_POST["fBookingId"])) {
						$bookingId = $_POST["fBookingId"];
						$db->exec("INSERT OR REPLACE INTO booking(bookingId, borrowerName, startTime, endTime, roomId, projectorId) VALUES (\"".$bookingId."\", \"".$borrowerName."\", \"".$startTime."\", \"".$endTime."\", \"".$room."\", \"".$projector."\")");
					} else {
						$db->exec("INSERT INTO booking(borrowerName, startTime, endTime, roomId, projectorId) VALUES (\"".$borrowerName."\", \"".$startTime."\", \"".$endTime."\", \"".$room."\", \"".$projector."\")");
					}
				} else {
					$bookingId = $_POST["bookingId"];
					$statement = $db->prepare('DELETE FROM booking WHERE bookingId =:bookingId');
					$statement->bindValue(':bookingId', $bookingId);
					$statement->execute();
				}
			}
			header( 'Location: index.php' ) ;
		}
		$db->close();
		die();
	}

	function showRooms() { 
		$db = new SQLite3('db/mysqlitedb.db');
		$results = $db->query('SELECT roomId FROM room');
		while ($row = $results->fetchArray()) {
			echo "<option value='Salle ".$row["roomId"]."'>Salle ".$row["roomId"]."</option>";
		}
		$db->close();
	}
	function showProjectors() { 
		$db = new SQLite3('db/mysqlitedb.db');
		$results = $db->query('SELECT projectorId FROM projector');
		while ($row = $results->fetchArray()) {
			echo "<option value='Projecteur ".$row["projectorId"]."'>Projecteur ".$row["projectorId"]."</option>";
		}
		$db->close();
	}
?>