<?php
	// this page feed the fullcalendar with all events into db, we give the title and color here
	function giveTitle($rowT) { 
		// give the right title to the event
		$time = ($rowT["endTime"] - $rowT["startTime"])/60;
		if($time >= 120) {
			return $rowT["borrowerName"]."\n Projecteur ".$rowT["projectorId"]."\n Salle ".$rowT["roomId"];
		}
		if($time >= 90) {
			return $rowT["borrowerName"]."\n Proj. ".$rowT["projectorId"].", Salle ".$rowT["roomId"];
		}
		return $rowT["borrowerName"].", P. ".$rowT["projectorId"].", S. ".$rowT["roomId"];
	}
	$db = new SQLite3('db/mysqlitedb.db');

	$statement = $db->prepare('SELECT * FROM booking WHERE startTime > :start and endTime < :end');
	$statement->bindValue(':start', $_GET["start"]);
	$statement->bindValue(':end', $_GET["end"]);

	$results = $statement->execute();
	$data[] = '';
	while ($row = $results->fetchArray()) {	
		if($row["projectorId"] === 1) {
			$color = "#0a2b72"; // Blue color
		} else {
			$color = "#138113"; // green color
		}
		$data[] = array("allDay" => false, "title" => giveTitle($row), "id" => $row["bookingId"],
		 "end" => $row["endTime"], "start" => $row["startTime"], "color" => $color);// we return the Json
	}
	echo json_encode($data);		
	$db->close();
?>